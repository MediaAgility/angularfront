import { Component, OnInit } from '@angular/core';
import { Deudor } from './deudor';
import { DeudorService } from './deudor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-deudores',
  templateUrl: './deudores.component.html'
})
export class DeudoresComponent implements OnInit {

  deudores: Deudor[];

  constructor(private deudorService: DeudorService) { }

  ngOnInit() {
    this.deudorService.getDeudores().subscribe(
      deudores => this.deudores = deudores
    );
  }

  // ELIMINAR DEUDOR
  delete(deudor: Deudor): void {
    Swal.fire({
      title: 'Qieres eliminar??',
      text: `Eliminaras al deudor ${deudor.nombre} ${deudor.apellido}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminarlo!'
    }).then((result) => {
      if (result.value) {
        this.deudorService.delete(deudor.id).subscribe(
          response => {
            this.deudores = this.deudores.filter(cli => cli !== deudor)
            Swal.fire(
              'Deudor Eliminado!',
              `Deudor ${deudor.nombre} ${deudor.apellido} Eliminado exitosamente`,
              'success'
            )
          }
        )
      }
    })
  }

}

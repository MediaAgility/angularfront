import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Deudor } from './deudor';



@Injectable()
export class DeudorService {
  private urlEndpoint: string = 'http://localhost:8080/api/deudores';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }


  // Listado de deudores
  getDeudores(): Observable<Deudor[]> {
    return this.http.get<Deudor[]>(this.urlEndpoint);
  }

  // Crear un nuevo Deudor
  create(deudor: Deudor): Observable<Deudor> {
    return this.http.post<Deudor>(this.urlEndpoint, deudor, { headers: this.httpHeaders })
  }

  // GET DEUDOR Y ID -> UTILIZADO PARA UPDATE
  getDeudor(id): Observable<Deudor> {
    return this.http.get<Deudor>(`${this.urlEndpoint}/${id}`)
  }

  // UPDATE
  update(deudor: Deudor): Observable<Deudor> {
    return this.http.put<Deudor>(`${this.urlEndpoint}/${deudor.id}`, deudor, { headers: this.httpHeaders })
  }

  // DELETE
  delete(id: number): Observable<Deudor> {
    return this.http.delete<Deudor>(`${this.urlEndpoint}/${id}`, { headers: this.httpHeaders })
  }



}

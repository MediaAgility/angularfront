import { Component, OnInit } from '@angular/core';
import { Deudor } from './deudor';
import { DeudorService } from './deudor.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  titulo: string = 'Nuevo deudor';
  private deudor: Deudor = new Deudor();

  constructor(
    private deudorService: DeudorService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.cargarDeudor()
  }

  // Conectarse al API REST
  // 1.Crear el metodo crear nuevo deudor en DeudorService
  // 2. inyectar la clase service de arriba para usar el metodo


  // CREAR
  public create(): void {
    // Invocar el metodo create del Service
    this.deudorService.create(this.deudor).subscribe(
      // Redirigir al lisado => Importar Router para hacerlo posible
      response => {
        this.router.navigate(['/deudores'])
        Swal.fire({
          position: 'top-end',
          type: 'success',
          title: `Deudor ${response.nombre} fue creado exitosamente!!`,
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  // CARGAR 1 DEUDOR
  // 2.Para que funcione hay que ponerlo en el onit

  cargarDeudor(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.deudorService.getDeudor(id).subscribe((cliente) => this.deudor = cliente)
      }
    })
  }



  // UPDATE
  update(): void {
    this.deudorService.update(this.deudor).subscribe(deudor => {
      this.router.navigate(['/deudores'])
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: `Deudor ${deudor.nombre} fue actualizado exitosamente!!`,
        showConfirmButton: false,
        timer: 2000
      })
    })
  }



}

export class Deudor {

  id: number;
  nombre: string;
  apellido: string;
  email: string;
  direccion: string;
  telefono: string;
  deuda: string;

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { DeudoresComponent } from './deudores/deudores.component';
import { DeudorService } from './deudores/deudor.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './deudores/form.component';
import { FormsModule } from '@angular/forms';




// Rutas
const routes: Routes = [
  { path: '', redirectTo: '/deudores', pathMatch: 'full' },
  { path: 'deudores', component: DeudoresComponent },
  { path: 'deudores/form', component: FormComponent },
  { path: 'deudores/form/:id', component: FormComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    DeudoresComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    DeudorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
